# Neovim Config

This is my neovim config.

## What you need

`neovim 0.6` pretty obvious dependency
`fzf` for finding stuff
`packer.nvim` [https://github.com/wbthomason/packer.nvim](Click this link to see the installation instructions)

## How to install
```sh
git clone https://git.envs.net/jasons393/nvim-config.git
cp -r nvim-config ~/.config/nvim
```

This is really meant for backup proposes but you can use this as an example.
