-- Plugins

local use = require("packer").use

vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
	-- Packer
	use({ 'wbthomason/packer.nvim' })
	
	-- Impatient
	use({'lewis6991/impatient.nvim'})
	
	-- Base16 theme
	use({ 'RRethy/nvim-base16' })

	-- Key Mapping
	use({ 'b0o/mapx.nvim' })

	-- Which key
	use({ 'folke/which-key.nvim' })

	-- Telescope
	use({ 'nvim-telescope/telescope.nvim', requires = {'nvim-lua/plenary.nvim'}, cmd = {'Telescope'},
	config = function()
		require ('config/telescope')
	end,
	})

	-- Wilder
	use({ 'gelguy/wilder.nvim',
	config = function()
		require ('config/wilder')
	end,
	})

	-- Dashboard
	use({ 'glepnir/dashboard-nvim',
	config = function()
		require ('config/dashboard')
	end,
	})

	-- Lualine
	use({ 'nvim-lualine/lualine.nvim', requires = { 'kyazdani42/nvim-web-devicons', opt = true },
	config = function()
		require ('config/lualine')
	end,
	})
end)
