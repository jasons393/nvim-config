-- Colorscheme
vim.cmd [[colorscheme base16-bright]]

-- Line numbers
vim.opt.number = true

-- Mouse
vim.opt.mouse = 'a'
